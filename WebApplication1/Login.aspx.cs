﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Helper;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Initializing of Database
            //AccountTableHandle.AddAcount("Frederikmere", "fred", "Fredabc123");
            //AccountTableHandle.AddAcount("Learkemere", "lear", "Learabc123");
            //AccountTableHandle.AddAcount("Akselmere", "akse", "Akseabc123");
            //AccountTableHandle.AddAcount("Madsmere", "mads", "Madsabc123");
            //AccountTableHandle.AddAcount("Mathildemere", "math", "Mathabc123");

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if ((this.UserName.Text.Trim().ToString().Length == 0) || (this.Password.Text.Trim().ToString().Length == 0))
            {
                MessageBox.Show(this.Page, "Udfyld Username og Password");
            }

            if (Session["loginFailureError"] == null)
            {
                Session["loginFailureError"] = 0;
            }
            if (Convert.ToInt32(Session["loginFailureError"]) > 5)
            {
                MessageBox.Show(this.Page, "Stop");
            }

            bool isLogin = AccountTableHandle.IsAccountExistsByUserNameAndPW(this.UserName.Text.Trim().ToString(), this.Password.Text.Trim().ToString());
            if (isLogin)
            {
                MessageBox.Show(this.Page, "Velkomme");

            }
            else
            {
                MessageBox.Show(this.Page, "Ugyldig Username og Password");
                Session["loginFailureError"] = Convert.ToInt32(Session["loginFailureError"]) + 1;
            }
        }
    }
}