﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplication1.Helper
{
    public class AccountTableHandle
    {
        public static void AddAcount(string _name, string _userName, string _passWord)
        {
            byte[] saltingToHash = Pbkdf2.GenerateSalt();
            var hashedPassword = Pbkdf2.HashPassword(Encoding.UTF8.GetBytes(_passWord),
                                                     saltingToHash,
                                                     1000);
            string hashedPasswordString = Convert.ToBase64String(hashedPassword);

            string sql = "INSERT INTO SoftewareTest_Account (Name, UserName, PassWord, Salting) VALUES (@Name, @UserName, @PassWord, @Salting)";
            SqlParameter[] myparm = new SqlParameter[4];
            myparm[0] = new SqlParameter("@Name", SqlDbType.NVarChar);
            myparm[0].Value = _name;
            myparm[1] = new SqlParameter("@UserName", SqlDbType.NVarChar);
            myparm[1].Value = _userName;
            myparm[2] = new SqlParameter("@PassWord", SqlDbType.NVarChar);
            myparm[2].Value = hashedPasswordString;
            myparm[3] = new SqlParameter("@Salting", SqlDbType.Binary);
            myparm[3].Value = saltingToHash;
            int returnRecord = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringLocalTransaction, CommandType.Text, sql, myparm);
        }

        public static byte[] GetSaltingByID(int _id)
        {
            string sql = "Select * FROM SoftewareTest_Account where ID = " + _id.ToString();
            SqlParameter[] myparm = new SqlParameter[0];
            SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.Text, sql, myparm);
            DataTable dt = new DataTable();
            dt.Load(mydr);
            if (dt.Rows.Count > 0)
            {
                return (byte[])dt.Rows[0]["Salting"];
            }
            else
            {
                return null;
            }

        }

        public static bool IsAccountExistsByUserNameAndPW(string _userName, string _passWord)
        {
            string sql = "Select * FROM SoftewareTest_Account where UserName = '" + _userName.Trim().ToString() + "'";
            SqlParameter[] myparm = new SqlParameter[0];
            SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.Text, sql, myparm);
            DataTable dt = new DataTable();
            dt.Load(mydr);

            if (dt.Rows.Count <= 0)
            {
                return false;
            }
            byte[] returnSaltingToHash = (byte[])dt.Rows[0]["Salting"];
            var hashedPassword = Pbkdf2.HashPassword(Encoding.UTF8.GetBytes(_passWord),
                                                     returnSaltingToHash,
                                                     1000);
            string hashedPasswordString = Convert.ToBase64String(hashedPassword);

            if (
                dt.Rows[0]["PassWord"].ToString().Trim() == hashedPasswordString.Trim().ToString()
                )
            {
                return true;
            }

            return false;

        }

    }
}