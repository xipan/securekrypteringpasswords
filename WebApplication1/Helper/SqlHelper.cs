﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Database common access code
/// Such abstract class, does not allow instantiation, can be called directly in the application
/// </summary>
public abstract class SqlHelper
{
    //Get the database connection string, which is a static variable and read-only, all the documents in the project can be used directly, but can not be modified
    public static readonly string ConnectionStringLocalTransaction = ConfigurationManager.ConnectionStrings["pubsConnectionString"].ConnectionString;

    // Hash table is used to store cached parameter information, the hash table can store any type of parameter.
    private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());

    /// <summary>
    ///Execute a SqlCommand command that does not require a return value by specifying a private connection string.
    /// Arguments Arrays provide a list of parameters
    /// </summary>
    /// <remarks>
    /// Use example
    ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid database connection string</param>
    /// <param name="commandType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>Returns a number indicating the number of rows affected by this SqlCommand command</returns>
    public static int ExecuteNonQuery(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {

        SqlCommand cmd = new SqlCommand();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            //Through the PrePareCommand method parameters will be added one by one to the SqlCommand parameter set
            PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
            int val = cmd.ExecuteNonQuery();

            //Clear the list of parameters in the SqlCommand
            cmd.Parameters.Clear();
            return val;
        }
    }

    /// <summary>
    ///Execute a SqlCommand that does not return a result, through an existing database connection
    /// Arguments Arrays provide parameters
    /// </summary>
    /// <remarks>
    /// Use example：  
    ///  int result = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="conn">A valid database connection string</param>
    /// <param name="commandType">SqlCommand command type (stored procedure, T-SQL statement, and so on.</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>Returns a number indicating the number of rows affected by this SqlCommand command</returns>
    public static int ExecuteNonQuery(SqlConnection connection, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {

        SqlCommand cmd = new SqlCommand();

        PrepareCommand(cmd, connection, null, cmdType, cmdText, commandParameters);
        int val = cmd.ExecuteNonQuery();
        cmd.Parameters.Clear();
        return val;
    }

    /// <summary>
    /// Executes a SqlCommand that does not return a result, through an existing database transaction 
    /// Arguments Arrays provide parameters
    /// </summary>
    /// <remarks>
    /// Use example：  
    ///  int result = ExecuteNonQuery(trans, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="trans">An existing SQL transaction processing</param>
    /// <param name="commandType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>Returns a number indicating the number of rows affected by this SqlCommand command</returns>
    public static int ExecuteNonQuery(SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {
        SqlCommand cmd = new SqlCommand();
        PrepareCommand(cmd, trans.Connection, trans, cmdType, cmdText, commandParameters);
        int val = cmd.ExecuteNonQuery();
        cmd.Parameters.Clear();
        return val;
    }

    /// <summary>
    /// Executes a SqlCommand command that returns the result set via a dedicated connection string.
    /// Arguments Arrays provide parameters
    /// </summary>
    /// <remarks>
    /// Use example： 
    ///  SqlDataReader r = ExecuteReader(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid database connection string</param>
    /// <param name="commandType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>Returns a SqlDataReader containing the result</returns>
    public static SqlDataReader ExecuteReader(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(connectionString);

        // Try / catch is used here because SqlDataReader does not exist if the method is out of order,
        //CommandBehavior.CloseConnection statement will not be executed, the trigger exception caught by the catch.
        //Closes the database connection and throws the caught exception again by throwing.
        try
        {
            PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            cmd.Parameters.Clear();
            return rdr;
        }
        catch
        {
            conn.Close();
            throw;
        }
    }

    /// <summary>
    /// Executes a SqlCommand command that returns the first column of the first record, through a dedicated connection string. 
    /// Arguments Arrays provide parameters
    /// </summary>
    /// <remarks>
    /// Use example：  
    ///  Object obj = ExecuteScalar(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="connectionString">A valid database connection string</param>
    /// <param name="commandType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>To return an object of type data, convert the type via the Convert.To {Type} method</returns>
    public static object ExecuteScalar(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {
        SqlCommand cmd = new SqlCommand();

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            PrepareCommand(cmd, connection, null, cmdType, cmdText, commandParameters);
            object val = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            return val;
        }
    }

    /// <summary>
    /// Executes a SqlCommand command that returns the first column of the first record, through an existing database connection.
    /// Arguments Arrays provide parameters
    /// </summary>
    /// <remarks>
    /// Use example：  
    ///  Object obj = ExecuteScalar(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
    /// </remarks>
    /// <param name="conn">An existing database connection</param>
    /// <param name="commandType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="commandText">The name of the stored procedure or T-SQL statement</param>
    /// <param name="commandParameters">Provides a list of parameters used in the SqlCommand command as an array</param>
    /// <returns>To return an object of type data, convert the type via the Convert.To {Type} method</returns>
    public static object ExecuteScalar(SqlConnection connection, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
    {

        SqlCommand cmd = new SqlCommand();

        PrepareCommand(cmd, connection, null, cmdType, cmdText, commandParameters);
        object val = cmd.ExecuteScalar();
        cmd.Parameters.Clear();
        return val;
    }

    /// <summary>
    /// Cache parameter array
    /// </summary>
    /// <param name="cacheKey">Parameter cache key</param>
    /// <param name="cmdParms">List of cached parameters</param>
    public static void CacheParameters(string cacheKey, params SqlParameter[] commandParameters)
    {
        parmCache[cacheKey] = commandParameters;
    }

    /// <summary>
    /// Get the cached parameters
    /// </summary>
    /// <param name="cacheKey">The KEY value used to find the parameter</param>
    /// <returns>Returns the cached array of parameters</returns>
    public static SqlParameter[] GetCachedParameters(string cacheKey)
    {
        SqlParameter[] cachedParms = (SqlParameter[])parmCache[cacheKey];

        if (cachedParms == null)
            return null;

        //Create a clone list of parameters
        SqlParameter[] clonedParms = new SqlParameter[cachedParms.Length];

        //Through the loop for the cloning parameter list assignment
        for (int i = 0, j = cachedParms.Length; i < j; i++)
            //Use the clone method to copy parameters in the parameter list
            clonedParms[i] = (SqlParameter)((ICloneable)cachedParms[i]).Clone();

        return clonedParms;
    }

    /// <summary>
    /// Prepare parameters for executing the command
    /// </summary>
    /// <param name="cmd">SqlCommand command</param>
    /// <param name="conn">Existing database connection</param>
    /// <param name="trans">Database transaction processing</param>
    /// <param name="cmdType">SqlCommand command types (stored procedures, T-SQL statements, etc.)</param>
    /// <param name="cmdText">Command text, T-SQL statements such as Select * from Products</param>
    /// <param name="cmdParms">Return the command with parameters</param>
    private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
    {

        //Judge the database connection status
        if (conn.State != ConnectionState.Open)
            conn.Open();

        cmd.Connection = conn;
        cmd.CommandText = cmdText;

        //Determine the need for things
        if (trans != null)
            cmd.Transaction = trans;

        cmd.CommandType = cmdType;

        if (cmdParms != null)
        {
            foreach (SqlParameter parm in cmdParms)
                cmd.Parameters.Add(parm);
        }
    }
}